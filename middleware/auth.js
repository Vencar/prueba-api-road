const verificarToken = (req, res, next) => {
    let token = req.headers.token

    if (!token || token != 123) {
        return res.status(401).json({
            status: false,
            msg: "No estas autorizado"
        })
    }

    next()
}

module.exports = {
    verificarToken
}