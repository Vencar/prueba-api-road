process.env.PORT = process.env.PORT || 4000;

/* Dependencias necesarias */
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const mysql = require('mysql')

const app = express()

/* Middlewares */
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
const { verificarToken } = require('../middleware/auth')

/* Objeto para Conectar MYSQL */
const dataDB = {
    host: 'localhost',
    user: 'pruebas',
    password: 'pruebas_pruebas',
    database: 'pruebas_gdl'
}

/* Creamos conexion a la DB */
const connection = mysql.createConnection(dataDB);

connection.connect((err) => {
    if (err) console.log("Error al conectar a la DB", err)

    console.log("Conectado a db", connection.threadId);

})


app.get('/', (request, response) => {
    return response.status(200).json({
        status: true,
        msg: "Bienvenido al api rest"
    })
})

app.get('/productos/:termino', [verificarToken], (request, response) => {
    const termino = request.params.termino

    if (!termino) {
        return res.status(400).json({
            status: false,
            msg: `Es necesario proporcionar un término para la busqueda`
        });
    }

    connection.query(`CALL ListarProducto('${termino}');`, function(err, res) {
        if (err) {
            return response.status(400).json({
                status: false,
                msg: `Error al consultar la informacion ${err}`
            })
        }

        const data = res[0]

        if (data.length == 0) {
            return response.status(400).json({
                status: true,
                data,
                msg: `No se encontraron resultados para el término: ${termino}`
            })
        }

        return response.status(200).json({
            status: true,
            data
        })
    })

})

app.listen(process.env.PORT, () => {
    console.log('Escuchando en el puerto', process.env.PORT);
});