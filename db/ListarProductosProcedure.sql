CREATE PROCEDURE `ListarProducto`(
	in _codigo varchar(45)
)
BEGIN
	SELECT 
		id, codigo, sku, descripcion 
	FROM 
		productos 
	WHERE 
		codigo LIKE CONCAT('%', _codigo , '%') 
	OR 
		sku LIKE CONCAT('%', _codigo , '%');
END